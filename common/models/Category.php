<?php

namespace common\models;

use Yii;
use yii\db\Query;
use Stringy\Stringy;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property integer $category
 * @property string $title
 * @property string $slug
 */
class Category extends \yii\db\ActiveRecord
{
    const GENERAL_CATEGORY_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['slug', 'filter', 'filter' => function($value) {
                if ( $value === '' ) {
                    $value = (string)Stringy::create($this->title)->slugify();
                    if ( $value === '' ) $value = 'n-a';
                }
                return $value;
            }],
            [['category'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 30],
            ['slug', 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/'],
            ['slug', function($attribute, $params) {
                $category_id = !empty($this->category) ? $this->category : NULL;
                $category = Category::findOne(['category' => $category_id, 'slug' => $this->slug]);
                if ($category instanceof Category && $category->id != $this->id ) {
                    $this->addError('slug', 'The combination of Category and Slug has already been taken.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'title' => 'Title',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        $categories = $this->hasOne(Category::className(), ['id'=>'category'])->all();
        return is_array($categories) && count($categories) > 0 ? $categories[0] : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('{{%posts_categories}}', ['category_id' => 'id']);
    }

    /**
     * Return all categories as "flat tree" in array
     *
     * @param null $category
     * @param null $categories
     * @param int $level
     * @param string $titlePrefix
     * @param string $url
     * @return array
     */
    public static function getCategoriesTreeArray($category = null, $categories = null, $level = 0, $titlePrefix = '&nbsp;&nbsp;', $url = '')
    {
        $tree = [];
        if ( $categories == null ) {
            $categories = Category::find()
                ->where(['category' => null])
                ->orderBy('title')
                ->all();
        }
        if ( is_numeric($category) ) {
            $category = Category::findOne(['id' => $category]);
        }

        /** @var $c Category */
        foreach($categories as $c) {
            if ( $category instanceof Category && $c->id == $category->id ) continue;
            $tree[] = [
                'id' => $c->id,
                'title' => str_repeat($titlePrefix, $level*2) . htmlspecialchars($c->title),
                'url' => $url . '/' . $c->slug,
                'level' => $level,
            ];
            $subCategories =  $c->getCategories()->all();
            if ( is_array($subCategories) && count($subCategories) > 0 ) {
                $tree = array_merge(
                    $tree,
                    Category::getCategoriesTreeArray(
                        $category,
                        $subCategories,
                        $level+1,
                        $titlePrefix,
                        $url . '/' . $c->slug
                    )
                );
            }
        }
        return $tree;
    }

    /**
     * Return non-empty categories as "flat tree" in array
     *
     * @return array
     */
    public static function getNonEmptyCategoriesTreeArray()
    {
        // TODO: cache it;

        $tree = [];
        $categoriesIds = self::getNonEmptyCategoriesIds();
        foreach(self::getCategoriesTreeArray(null, null, 0, '') as $category) {
            if (in_array($category['id'], $categoriesIds)) $tree[] = $category;
        }
        return $tree;
    }

    /**
     * Get IDs of all non-empty categories with parent categories tree
     *
     * @param mixed $categoriesIds
     * @return array
     */
    protected static function getNonEmptyCategoriesIds($categoriesIds=null)
    {
        $ids = [];
        if (empty($categoriesIds)) {
            $sql = "SELECT DISTINCT pc.category_id
                      FROM {{%posts}} p
                      JOIN {{%posts_categories}} pc ON (p.id = pc.post_id)";
            $categoriesIds = Yii::$app->db
                ->createCommand($sql)
                ->queryColumn();
            if (count($categoriesIds) == 1 && empty($categoriesIds[0])) $categoriesIds = [];
        } elseif (!is_array($categoriesIds)) {
            $categoriesIds = [$categoriesIds];
        }
        /** @var $c Category */
        foreach(Category::findByCondition(['id' => $categoriesIds], false) as $c) {
            $ids[] = $c->id;
            $parentCategory = $c->getCategory();
            if ($parentCategory instanceof Category) {
                $ids = array_merge($ids, self::getNonEmptyCategoriesIds($parentCategory->id));
            }
        }
        return array_unique($ids);
    }

    /**
     * Return all child categories IDs of $category
     *
     * @param Category $category
     * @return array
     */
    public static function getChildCategoriesIds(Category $category)
    {
        $ids = [$category->id];
        /** @var $c Category */
        foreach($category->getCategories()->all() as $c) {
            $ids[] = $c->id;
            $childCategories = $c->getCategories()->all();
            if (is_array($childCategories) && count($childCategories) > 0)
                $ids = array_merge($ids, self::getChildCategoriesIds($c));
        }
        return array_unique($ids);
    }

    /**
     * Return posts by category with child categories tree
     *
     * @param Category $category
     * @return \yii\db\ActiveQuery
     */
    public static function getAllPosts(Category $category)
    {
        return Post::find()
            ->leftJoin('{{%posts_categories}}', '{{%posts_categories}}.post_id = {{%posts}}.id')
            ->where('{{%posts_categories}}.category_id IN ('.implode(',', Category::getChildCategoriesIds($category)).')');
    }

}
