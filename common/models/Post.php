<?php

namespace common\models;

use Yii;
use Stringy\Stringy;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%posts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['slug', 'filter', 'filter' => function($value) {
                if ( $value === '' ) {
                    $value = (string)Stringy::create($this->title)->slugify();
                    if ( $value === '' ) $value = 'n-a';
                }
                return $value;
            }],
            ['created_at', 'filter', 'filter' => function($value) {
                if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/', $value)) {
                    $value = strtotime($value);
                }
                return $value;
            }],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 30],
            [['slug'], 'unique'],
            [['slug'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/'],            
            [['content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('{{%posts_categories}}', ['post_id' => 'id']);
    }

    /**
     * Return coma separated categories of post
     *
     * @return string
     */
    public function getCategoriesInText()
    {
        $categoreis = [];
        foreach($this->getCategories()->all() as $c) {
            $categoreis[] = $c->title;
        }
        return implode(', ', $categoreis);
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ( empty($this->created_at) ) $this->created_at = time();

        if (parent::save($runValidation, $attributeNames)) {
            $this->updated_at = time();
            $this->update($runValidation, $attributeNames);
            $this->unlinkAll('categories');
            $categoriesIds = Yii::$app->getRequest()->getBodyParam('categories_list');
            if ( !is_array($categoriesIds) || count($categoriesIds) < 1)
                $categoriesIds[] = Category::GENERAL_CATEGORY_ID;
            foreach(Category::findByCondition(['id' => $categoriesIds], false) as $c) {
                $this->link('categories', $c);
            }
            return true;
        } 

        return false;
    }

    /**
     * Return the excerpt of post content
     *
     * @param int $size
     * @return string
     */
    public function getExcerpt($size = 600)
    {
        $excerpt = strip_tags($this->content);
        $length = mb_strlen($excerpt, 'UTF-8');
        if ($length > $size) {
            $pos = mb_strpos($excerpt, ' ', $size, 'UTF-8');
            if ($pos !== false) $excerpt = mb_substr($excerpt, 0, $pos, 'UTF-8') . '...';
        }
        return $excerpt;
    }

}
