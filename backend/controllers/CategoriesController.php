<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Category;

/**
 * CategoriesController implements the CRUD actions for Category model.
 */
class CategoriesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($id == Category::GENERAL_CATEGORY_ID) {
            Yii::$app->getSession()->setFlash('info', 'You can\'t delete this category');
        } else {
            $category = Category::findOne(['id' => $id]);
            if ($category instanceof Category) {
                // move posts to General category
                $ids = $this->getCategoryChildIds($category);
                $ids[] = $id;
                $sql = "UPDATE {{%posts_categories}}
                           SET category_id = ".Category::GENERAL_CATEGORY_ID."
                         WHERE category_id IN (".implode(',', $ids).")";
                Yii::$app->db->createCommand($sql)->execute();
                foreach($ids as $id) {
                    $this->findModel($id)->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Return ids of all child categories of $category
     *
     * @param Category $category
     * @return array
     */
    protected function getCategoryChildIds(Category $category)
    {
        $ids = [];
        /** @var $c Category */
        foreach($category->getCategories()->all() as $c) {
            $ids[] = $c->id;
            $childCategories = $c->getCategories()->all();
            if ( is_array($childCategories) && count($childCategories) > 0)
                $ids = array_merge($ids, $this->getCategoryChildIds($c));
        }
        return $ids;
    }
}
