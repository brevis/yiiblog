<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;
use dosamigos\datetimepicker\DateTimePicker;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#post-categories label{
    display: block;
    font-weight: normal;
}
</style>
<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 10],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($model, 'created_at')->widget(DateTimePicker::className(), [
                'size' => 'ms',
                'template' => '{input}',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'clientOptions' => [
                    'startView' => 1,
                    'minView' => 0,
                    'maxView' => 1,
                    'autoclose' => true,
                    'linkFormat' => 'yyyy-mm-dd HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]) ?>

        </div>

        <div class="col-lg-4">
            <div class="form-group field-post-categories">
                <?= htmlspecialchars_decode($form->field($model, 'categories')->checkboxList(ArrayHelper::map(Category::getCategoriesTreeArray($model->id), 'id', 'title'), ['name'=>'categories_list'])) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>