<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= htmlspecialchars_decode(GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => Category::getCategoriesTreeArray(),
            'key' => 'id',
            'pagination' => [
                'pageSize' => 9999,
            ],
        ]),
        'columns' => [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'title',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ])); ?>

</div>