<?php

use yii\db\Schema;
use yii\db\Migration;

class m150205_083447_update_users_role extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'role', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->alterColumn('{{%user}}', 'role');
    }
}
