<?php

use yii\db\Schema;
use yii\db\Migration;

class m150205_085221_create_posts_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%posts}}', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'slug' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
            'UNIQUE (slug)'
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%posts}}');
    }
}
