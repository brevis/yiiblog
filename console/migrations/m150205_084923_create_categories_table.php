<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Category;

class m150205_084923_create_categories_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%categories}}', [
            'id' => 'pk',
            'category' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'slug' => Schema::TYPE_STRING . ' NOT NULL',
            'UNIQUE (category, slug)'
        ]);
        $this->insert('{{%categories}}', ['id' => Category::GENERAL_CATEGORY_ID, 'title' => 'General', 'slug' => 'general']);
    }

    public function down()
    {
        $this->dropTable('{{%categories}}');
    }
}
