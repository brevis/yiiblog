<?php

use yii\db\Schema;
use yii\db\Migration;

class m150207_120551_create_posts_categories_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%posts_categories}}', [
            'post_id' => Schema::TYPE_INTEGER,
            'category_id' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%posts_categories}}');
    }
}
