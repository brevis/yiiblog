<?php
namespace console\controllers;

use common\models\User;
use \yii\console\Controller;
use yii\helpers\BaseConsole;

class UserRoleController extends Controller
{
    public function actionMakeAdmin($username)
    {
        $this->changeUserRole($username, User::ROLE_ADMIN);
    }

    public function actionMakeRegular($username)
    {
        $this->changeUserRole($username, User::ROLE_USER);
    }

    protected function changeUserRole($username, $role)
    {
        $user = User::findOne(['username' => $username]);
        if (!$user instanceof User) {
            $this->stdout("User with username \"$username\" not found.\n", BaseConsole::FG_RED);
            return Controller::EXIT_CODE_ERROR;
        }
        $user->role = $role;
        $user->save();
        $this->stdout("Done.\n", BaseConsole::FG_GREEN);
        return Controller::EXIT_CODE_NORMAL;
    }
 }