Stupid Yii 2 Blog App
================

![yiiblog.png](https://bitbucket.org/repo/4jMgqM/images/2232350268-yiiblog.png)

### Installation

```
#!bash
# 1. Clone repo.
git clone git@bitbucket.org:brevis/yiiblog.git
cd yiiblog

# 2. Create two virtual hosts:
# frontend: with document_root pointed to /path/to/yiiblog/frontend/web and using the URL http://frontend/
# backend: with document_root pointed to /path/to/yiiblog/backend/web and using the URL http://backend/

# 3. Install dependencies.
curl -sS https://getcomposer.org/installer | php
php composer.phar global require "fxp/composer-asset-plugin:1.0.0"
php composer.phar install

# 4. Init yii2 app.
./init

# 5. Provide your database connection settings.
vim common/config/main-local.php 

# 6. Run DB migration.
./yii migrate/up

# 7. Open frontend URL in web-browser, goto "Signup" page and register new account.

# 8. Grant admin role to your account.
./yii user-role/make-admin %your_username%

# 9. Enjoy!
```

### Known sad problems
1. Not working datatimepicker widget in production environment.
Possible solution: nada u Romi sprashivat'.

2. ...