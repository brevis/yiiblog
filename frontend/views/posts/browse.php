<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */

$this->title = implode(' / ', $siteTitle);
if (!empty($breadcrumbs)) {
    $breadcrumbs[count($breadcrumbs)-1] = $breadcrumbs[count($breadcrumbs)-1]['label'];
    $this->params['breadcrumbs'] = $breadcrumbs;
}

rmrevin\yii\fontawesome\AssetBundle::register($this);
?>
<h1><?= end($siteTitle) ?></h1>

<?php if (count($posts) > 0) : ?>
    <?php foreach ($posts as $post) : ?>
    <div class="post">
        <h2><a href="<?= Url::to('/post/' . $post->slug) ?>"><?= $post->title ?></a></h2>
        <p class="postdate"><?= FA::icon('calendar') ?> <?= date('d F Y, H:i', $post->created_at) ?></p>
        <p><?= $post->getExcerpt() ?></p>
        <p class="categories"><?= FA::icon('tags') ?> <?= $post->getCategoriesInText() ?></p>
    </div>
    <?php endforeach; ?>
<?php else : ?>
    <p>Here is no posts yet :(</p>
<?php endif; ?>

<?= urldecode(LinkPager::widget([
    'pagination' => $pages,
])) ?>