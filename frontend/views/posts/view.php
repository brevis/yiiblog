<?php
use common\models\Post;
use rmrevin\yii\fontawesome\FA;
/* @var $this yii\web\View */

/* @var $post Post */
$this->title = $post->title;

$this->params['breadcrumbs'][] = $post->title;

rmrevin\yii\fontawesome\AssetBundle::register($this);
?>
<div class="post">
    <h1><?= $post->title ?></h1>
    <p class="postdate"><?= FA::icon('calendar') ?> <?= date('d F Y, H:i', $post->created_at) ?></p>
    <p><?= Yii::$app->formatter->asHtml($post->content) ?></p>
    <p class="categories"><?= FA::icon('tags') ?> <?= $post->getCategoriesInText() ?></p>
</div>