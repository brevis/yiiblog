<?php
namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Category;
use common\models\Post;

class PostsController extends \yii\web\Controller
{
    public $currentCategoryId = 0;

    public function actionBrowse($slug)
    {
        $siteTitle = [];
        $breadcrumbs = [];
        $slugArr = array_filter(explode('/', $slug), 'trim');

        /** @var $category Category */
        $category = null;

        if (count($slugArr) > 0) {
            /** @var $parentCategory Category */
            $parentCategory = null;
            $breadcrumbsUrl = '';
            foreach($slugArr as $part) {
                if ($parentCategory == null) {
                    $category = Category::findOne(['category' => null, 'slug' => $part]);
                } else {
                    $category = Category::findOne(['category' => $parentCategory->id, 'slug' => $part]);
                }
                if ($category instanceof Category) {
                    $breadcrumbsUrl .= '/'.$category->slug;
                    $breadcrumbs[] = [
                        'label' => $category->title,
                        'url' => $breadcrumbsUrl,
                    ];
                    $siteTitle[] = $category->title;
                    $parentCategory = $category;
                } else {
                    throw new \yii\web\HttpException(404, 'Page not found.');
                }
            }
            $this->currentCategoryId = $category->id;
        }

        if ($category instanceof Category) {
            $query = Category::getAllPosts($category);
        } else {
            $query = Post::find();
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('created_at DESC')
            ->all();

        return $this->render('browse', [
            'siteTitle' => $siteTitle,
            'breadcrumbs' => $breadcrumbs,
            'category' => $category,
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }

    public function actionView($slug)
    {
        $post = Post::findOne(['slug' => $slug]);
        if (!$post instanceof Post) {
            throw new \yii\web\HttpException(404, 'Page not found.');
        }
        return $this->render('view', [
            'post' => $post
        ]);
    }

}
